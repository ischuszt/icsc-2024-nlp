# iCSC 2024 - A Practical Guide to Modern NLP

This repository contains the code and instructions for all the exercises related the the NLP introduction course for iCSC 2024.

Please clone this in [SWAN](https://swan-k8s.cern.ch). Make sure you request a machine that has GPUs, so that you can experiment with your solutions.

For more advanced topics, such as tranformers and GPT algorithms, some very nice visual learning aids and lectures have been linked in [the state of the art chapter](3.%20State%20of%20the%20art%20(OPTIONAL%20&%20ADVANCED)).
